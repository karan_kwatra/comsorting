from django.contrib import admin
from solo.admin import SingletonModelAdmin
from django.contrib.admin import AdminSite

from .models import *

admin.site.register(Hospital)
admin.site.register(Preference)
admin.site.register(Assignment)
admin.site.register(SelectionDate)
# Register your models here.
