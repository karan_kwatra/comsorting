from django.db import models
from django.contrib.auth.models import User
from solo.models import SingletonModel

class Hospital(models.Model):
    name = models.CharField(max_length=200)
    capacity = models.IntegerField()
    size = models.IntegerField(default=0)
    
    def __str__(self):
        return self.name

    def update(self):
        self.size += 1
    
class Preference(models.Model):
    rank = models.IntegerField()
    hospital = models.ForeignKey(Hospital, on_delete=models.CASCADE)
    student = models.ForeignKey(User)
    dateupdated = models.DateTimeField()
    
    def __str__(self):
        return str(self.student)

class Assignment(models.Model):
    hospital = models.ForeignKey(Hospital, on_delete=models.CASCADE)
    student = models.ForeignKey(User)
    prefassigned = models.IntegerField(default=0)
    
    def __str__(self):
        return str(self.student)
    
class SelectionDate(SingletonModel):
    date = models.DateTimeField()
    
    def __str__(self):
        return str(self.date)
