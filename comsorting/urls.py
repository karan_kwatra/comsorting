"""comsorting URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url
from django.contrib import admin
from . import views
from django.conf.urls import include

urlpatterns = [
    url(r'^submit/$', views.submit),
    url(r'^submit/view/$', views.submitview2),
    url(r'^logout/$', views.Logout),
    url(r'^sort/$', views.sort),
    url(r'^home/$', views.Home),
    url(r'^$', views.index),
    url(r'^admin/', admin.site.urls), 
    url(r'^viewpreferences/', views.viewpreferences),
    url(r'^randompreferences/', views.randompreferences), 
    url(r'^changedate/',views.changedate),
    url(r'^results/', views.results),
    url(r'^upload/',views.upload),
    url(r'^changepassword/',views.changepassword),
    url(r'^resetpassword/',views.resetpassword),
    url(r'^downloadpreferences/',views.downloadpreferences),
    url(r'^downloadassignments/',views.downloadassignments),
    url(r'^about/',views.about),
    url(r'^assignment/',views.assignment),
    url(r'^', include('hide_herokuapp.urls'))
]