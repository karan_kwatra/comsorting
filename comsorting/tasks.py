from celery.task import task
from com.models import Assignment,Preference,Hospital
from django.contrib.auth.models import User
from celery import Celery
from django.shortcuts import render, redirect, render_to_response
app = Celery('tasks', broker='django://',include=['comsorting.tasks'], backend='djcelery.backends.database:DatabaseBackend')

@app.task
def match():
    print('sup')
    Assignment.objects.all().delete()
    Hospital.objects.all().update(size=0)
    num_assigned = 0
    all_students = User.objects.exclude(username='admin').order_by('?')
    student_count = all_students.count()
    all_hospitals = Hospital.objects.all()


    for i in range(1,9):
        for each_student in all_students:
            if Assignment.objects.filter(student=each_student).exists() is False:
                preferred_hospital = Preference.objects.get(student=each_student,rank=i).hospital
                if preferred_hospital.size < preferred_hospital.capacity:
                    Assignment.objects.create(hospital=preferred_hospital,student=each_student,prefassigned=i)
                    preferred_hospital.update()
                    preferred_hospital.save()
                    num_assigned+=1
                    print(num_assigned)

    unassigned = []
    assigned = []
    for each_student in all_students:
        if Assignment.objects.filter(student=each_student).exists() is False:
            unassigned.append(each_student)
        else:
            assigned.append(each_student)


    all_students = all_students.order_by('assignment__prefassigned')

    while num_assigned < student_count:
        print('stop')
        for other in all_students:
            if num_assigned == student_count:
                break
            if Assignment.objects.filter(student=other).exists() is True:
                others_assignment = Assignment.objects.get(student=other).hospital
                for i in range(1,10):
                    move_hospital = Preference.objects.get(student=other,rank=i).hospital
                    for student in unassigned:
                        for j in range(1,9):
                            if Preference.objects.get(student=student,rank=j).hospital == others_assignment:
                                if Assignment.objects.filter(student=student).exists() is False:
                                    if move_hospital.size < move_hospital.capacity:
                                        Assignment.objects.get(student=other).delete()
                                        Assignment.objects.create(student=other,hospital=move_hospital,prefassigned=i)
                                        move_hospital.update()
                                        move_hospital.save()
                                        Assignment.objects.create(student=student,hospital=others_assignment,prefassigned=j)
                                        num_assigned+=1
                                        print(num_assigned)
                                        print(str(other) + 'assigned to' + str(move_hospital))
                                        break  
                                    
    return redirect('/results')