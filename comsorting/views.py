from django import forms
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, render_to_response
from django.template import loader, Context, RequestContext
from django.template.loader import get_template
from django.core.mail import send_mail
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from .forms import HospitalForm, SelectionDateForm, UploadFileForm, UsernameForm, ChangePasswordForm
from pytz import timezone
from dateutil import tz
from datetime import datetime
from com.models import Preference, Assignment, SelectionDate, Hospital
from comsorting.tasks import match
import django_excel as excel
import random
import pytz

def index(request):
    next = request.GET.get('next', '/home/')

    if request.user.is_authenticated():
        return redirect('home/')

    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                if user.last_login is not None:
                    login(request, user)
                    return HttpResponseRedirect('/home/')
                else: 
                    login(request,user)
                    return HttpResponseRedirect('/changepassword')
            else:
                return HttpResponse("Inactive user.")
        else:
            return render(request, "index.html", {'redirect_to': '/', 'message':"Invalid Credentials!"})

    return render(request, "index.html", {'redirect_to': next})

@login_required
def submit(request):
    if datetime.now(pytz.timezone("America/New_York")) > SelectionDate.objects.get().date:
        return render(request, 'toolate.html')

    if request.method == 'POST':
        form1 = HospitalForm(request.POST)

        if datetime.now(pytz.timezone("America/New_York")) > SelectionDate.objects.get().date:
            return render(request, 'submit.html', {'message':"This form has closed, your submission was not saved, please wait for your results.",
                                                        'form1':form1
                })

        if form1.is_valid():

            #list of preferences
            preferences = []
            keys = form1.cleaned_data.keys()
            keys = sorted(keys,key=lambda k:(k[0], int(k[8:])) )
            
            for key in keys: 
                preferences.append(form1.cleaned_data[key])
            #make sure student didn't select two of the same preference
            seen = set()
            unique = not any(i in seen or seen.add(i) for i in preferences)

            if unique:
                Preference.objects.filter(student=request.user).delete()
                for i in range(0,len(preferences)):
                    Preference.objects.create(rank=i+1, student=request.user, hospital=preferences[i],dateupdated=datetime.now(pytz.timezone("America/New_York")))

                return redirect(submitview2)

            else:
                return render(request, 'submit.html', {'message':"Cannot select the same hospital for multiple preferences",
                                                        'form1':form1
                })

    else:
        student_prefs = Preference.objects.filter(student=request.user).order_by('rank')
        preferences = {}
        for i,pref in enumerate(student_prefs):
            preferences['hospital'+str(i+1)]=pref.hospital.id
        print(preferences)
        
        form1 = HospitalForm(initial=preferences)

    return render(request, 'submit.html', {'form1':form1})

@login_required
def submitview2(request):
    try:
        preferences = list(Preference.objects.filter(student=request.user).order_by('rank'))
        dateupdated = Preference.objects.get(rank=1,student=request.user).dateupdated
        eastern = timezone('US/Eastern')
        dateupdated = dateupdated.astimezone(eastern)
        dateupdated = dateupdated.strftime('%B %-d %Y %-I:%M:%S %p')
    except:
        dateupdated = None
        preferences = None
    return render(request, "print.html", {'preferences': preferences,'dateupdated':dateupdated})

def Logout(request):
    logout(request)
    return HttpResponseRedirect('/')

@login_required
def Home(request):

    selectiondate = SelectionDate.objects.get().date
    try:
        preferences = list(Preference.objects.filter(student=request.user).order_by('rank'))
        dateupdated = Preference.objects.get(rank=1,student=request.user).dateupdated
        eastern = timezone('US/Eastern')
        dateupdated = dateupdated.astimezone(eastern)
        dateupdated = dateupdated.strftime('%B %-d %Y %-I:%M:%S %p')
    except:
        dateupdated = None
        preferences = None

    return render(request, 'home.html', {'name': request.user.first_name,
                                            'selectiondate':selectiondate,
                                            'preferences':preferences, 
                                            'dateupdated':dateupdated
    })


@staff_member_required
def sort(request):
    if datetime.now(pytz.timezone("America/New_York")) > SelectionDate.objects.get().date:
        Assignment.objects.all().delete()
        match.delay()
        return render(request,'calculating.html')
        
    else:
        return render(request, 'error.html')

@staff_member_required
def viewpreferences(request):
    elements = []
    all_users = User.objects.exclude(username='admin')

    i=0
    for each_user in all_users:
        elements.append([])
        elements[i].append(each_user)
        preflist = list(Preference.objects.filter(student=each_user).order_by("rank"))
        if not len(preflist) == 0:
            dateupdated = preflist[0].dateupdated
            eastern = timezone('US/Eastern')
            dateupdated = dateupdated.astimezone(eastern)
            elements[i].append('Date Updated: ' + str(dateupdated.strftime('%B %-d %Y %-I:%M:%S %p')))
        for index in range(len(preflist)):
            elements[i].append(preflist[index].hospital.name)
        i+=1

    return render(request, 'sort.html', {'elements':elements})

@staff_member_required
def randompreferences(request):

    all_users = User.objects.exclude(username='admin')
    all_hospitals = Hospital.objects.all()
    Preference.objects.all().delete()
    
    for each_user in all_users:
        preferences = []
        randlist = random.sample(range(0, all_hospitals.count()), 10)
        for i in range(10):
            index = randlist[i]
            preferences.append(Preference.objects.create(rank=i+1,student=each_user,hospital=all_hospitals[index],dateupdated=datetime.now(pytz.timezone("America/New_York"))))

    return redirect(viewpreferences)

@staff_member_required
def changedate(request):
    if request.method == 'POST':
        form1 = SelectionDateForm(request.POST)

        if form1.is_valid():
            SelectionDate.objects.all().delete()
            SelectionDate.objects.create(date=form1.cleaned_data['date'])
            return redirect(Home)
    else:
        form1 = SelectionDateForm()

    return render(request, 'changedate.html', {'form1':form1})

@staff_member_required
def results(request):
    all_students = User.objects.exclude(username='admin')
    elements = []
    pref_assigned = [0,0,0,0,0,0,0,0,0,0]
    all_done = True
    
    for each_student in all_students:
        if Assignment.objects.filter(student=each_student).exists() is False:
            all_done = False

        print(each_student)
        
        try:
            assignment = Assignment.objects.get(student=each_student).hospital
            rank = Preference.objects.get(hospital=assignment,student=each_student).rank
            pref_assigned[rank-1]+=1
            elements.append('' + str(each_student) + ' was assigned to ' + assignment.name + ' this was preference ' + str(rank))
        except:
            elements.append('' + str(each_student) + 'was not assigneds')
    return render (request, 'results.html', {'elements':elements, 'pref_assigned':pref_assigned, 'all_done':all_done})
    
@staff_member_required    
def upload(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            User.objects.exclude(username='admin').delete()
            filehandle = request.FILES['file']
            if filehandle.name.endswith('.xlsx'):
                records =  filehandle.get_records()
                for record in records:
                    try:
                        username=record['MSU NetID']
                        password = User.objects.make_random_password()
                        email=record['MSU NetID']+'@gmail.com'
                        first_name=record['First Name']
                        last_name=record['Last Name']
                        new_user = User.objects.create_user(username,email,password)
                        new_user.first_name=first_name
                        new_user.last_name=last_name
                        new_user.save()
                        
                        send_mail(
                            'Your new account',
                            'Your password is: ' + str(password),
                            settings.EMAIL_HOST_USER,
                            [email],
                            fail_silently=False,
                        )
                        print('complete')
                         
                    except:
                        return render_to_response('upload_form.html', {'message':"Please upload a .xlsx file with the correct format",
                                                                'form': form}, context_instance=RequestContext(request))    
                                                                
                return HttpResponseRedirect('/viewpreferences')
                    
            else:
                return render_to_response('upload_form.html', {'message':"Please upload a .xlsx file with the correct format",
                                                                'form': form}, context_instance=RequestContext(request))    
    else:
        form = UploadFileForm()
    return render_to_response('upload_form.html', {'form': form}, context_instance=RequestContext(request))    
    
@login_required    
def changepassword(request):
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            user = request.user
            old_password = form.cleaned_data['oldpassword']
            if not user.check_password(old_password):
                return render(request, 'changepassword.html', {'message':"Your current password was incorrect",
                                                        'form':form
                })                
                
            password1 = form.cleaned_data['newpassword1']
            password2 = form.cleaned_data['newpassword2']
            
            if password1 != password2:
                return render(request, 'changepassword.html', {'message':"Passwords do not match",
                                                        'form':form
                })                   
            else:
                request.user.set_password(password1)
                request.user.save()
                return render(request, 'passwordchanged.html')
    else:
        form = ChangePasswordForm()

    return render(request, 'changepassword.html', {'form': form})
    
def resetpassword(request):
    if request.method == 'POST':
        form = UsernameForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['uname']
            if User.objects.all().filter(username=username).exists():
                password = User.objects.make_random_password()
                user = User.objects.get(username=username)
                user.set_password(password)
                user.save()
                send_mail(
                    'Your account has been reset',
                    'Your new password is: ' + str(password),
                    settings.EMAIL_HOST_USER,
                    [username+'@gmail.com'],
                    fail_silently=False,
                )                
                return render(request, 'emailsent.html')
            else:
                return render(request, 'resetpassword.html', {'message':"This username is not registered",
                                                        'form':form
                })                
    else:
        form = UsernameForm()

    return render(request, 'resetpassword.html', {'form': form})
    
@staff_member_required
def downloadpreferences(request):
    
    all_students = User.objects.exclude(username='admin')
    
    sheet = excel.pe.Sheet([[" "]])
    for i in range(10):
        sheet.column += ["Preference "+str(i+1)]
        
    for i,student in enumerate(all_students):
        preflist = [student.username]
        for j in range(10):
            try: 
                preflist.append(str(Preference.objects.get(rank=j+1,student=student).hospital))
            except: 
                preflist.append("-")
                
        sheet.row += preflist
        
    return excel.make_response(sheet, "xlsx", file_name="preferences")

@staff_member_required
def downloadassignments(request):
    all_students = User.objects.exclude(username='admin')
    
    sheet = excel.pe.Sheet([[" "]])
    for i in range(10):
        sheet.column += ["Preference "+str(i+1)]
    sheet.column+= ["Assignment"]
        
    for i,student in enumerate(all_students):
        preflist = [student.username]
        for j in range(10):
            try: 
                preflist.append(str(Preference.objects.get(rank=j+1,student=student).hospital))
            except: 
                preflist.append("-")
        try: 
            preflist.append(str(Assignment.objects.get(student=student).hospital))
        except: 
            preflist.append("-")
                
        sheet.row += preflist
        
    return excel.make_response(sheet, "xlsx", file_name="assignments")

def about(request):
    return render(request, 'about.html')

def assignment(request):
    try: 
        assignment = Assignment.objects.get(student=request.user).hospital
    except: 
        assignment = None
        
    return render(request, 'assignment.html', {'assignment':assignment})
    
