from django import forms
from com.models import Hospital, SelectionDate, Preference
from django.contrib.auth.models import User
from django.forms import Select
from datetime import datetime, date
from django.db import models
from django.contrib.admin.widgets import AdminDateWidget
import pytz
from pytz import timezone

class HospitalForm(forms.Form):
    
    attrs = {}

    hospital1 = forms.ModelChoiceField(
        queryset = Hospital.objects.all(), 
        label = 'Preference 1', 
        required=True,
        widget=Select(attrs=attrs))
    hospital2 = forms.ModelChoiceField(
        queryset = Hospital.objects.all(), 
        label = 'Preference 2',
        required=True,
        widget=Select(attrs=attrs))
    hospital3 = forms.ModelChoiceField(
        queryset = Hospital.objects.all(), 
        label = 'Preference 3',
        required=True,
        widget=Select(attrs=attrs))
    hospital4 = forms.ModelChoiceField(
        queryset = Hospital.objects.all(), 
        label = 'Preference 4',
        required=True,
        widget=Select(attrs=attrs))
    hospital5 = forms.ModelChoiceField(
        queryset = Hospital.objects.all(), 
        label = 'Preference 5',
        required=True,
        widget=Select(attrs=attrs))
    hospital6 = forms.ModelChoiceField(
        queryset = Hospital.objects.all(), 
        label = 'Preference 6',
        required=True,
        widget=Select(attrs=attrs))
    hospital7 = forms.ModelChoiceField(
        queryset = Hospital.objects.all(), 
        label = 'Preference 7',
        required=True,
        widget=Select(attrs=attrs))
    hospital8 = forms.ModelChoiceField(
        queryset = Hospital.objects.all(), 
        label = 'Preference 8',
        required=True,
        widget=Select(attrs=attrs))
    hospital9 = forms.ModelChoiceField(
        queryset = Hospital.objects.all(), 
        label = 'Preference 9',
        required=True,
        widget=Select(attrs=attrs))
    hospital10 = forms.ModelChoiceField(
        queryset = Hospital.objects.all(), 
        label = 'Preference 10',
        required=True,
        widget=Select(attrs=attrs))

        
    class Meta:
        model = Hospital
        fields = ['name']
        
class SelectionDateForm(forms.Form):
    date = forms.DateTimeField(widget=forms.SelectDateWidget(empty_label="Nothing"))
    
    class Meta: 
        model = SelectionDate

class UploadFileForm(forms.Form):
    file = forms.FileField()
    
class UsernameForm(forms.Form):
    uname = forms.CharField(label='Username', max_length=100)
    
class ChangePasswordForm(forms.Form):
    oldpassword = forms.CharField(label='Current password', widget=forms.PasswordInput())
    newpassword1 = forms.CharField(label='New password', widget=forms.PasswordInput())
    newpassword2 = forms.CharField(label='Retype new password', widget=forms.PasswordInput())